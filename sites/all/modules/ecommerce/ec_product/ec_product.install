<?php

/**
 * @file
 * Install and uninstall routines, incremental database updates and database
 * schema.
 */

/**
 * e-Commerce product module schema
 */
function ec_product_install() {
  $versions = drupal_get_installed_schema_version('product', FALSE, TRUE);
  if (!isset($versions['product']) || $versions['product'] == SCHEMA_UNINSTALLED) {
    drupal_install_schema('ec_product');
    variable_set('ec_product_fresh_install', TRUE);
  }

  $result = db_query('SELECT * FROM {permission} WHERE rid in (1,2)');
  while ($perm = db_fetch_array($result)) {
    if (stripos(', '. $perm['perm'], ', purchase products') === FALSE) {
      $perm['perm'] .= ', purchase products';
      drupal_write_record('permission', $perm, 'pid');
    }
  }
}

/**
 * Implementation of hook_uninstall().
 */
function ec_product_uninstall() {
  drupal_uninstall_schema('ec_product');
  // delete the variables
  foreach (node_get_types('names') as $type => $name) {
    variable_del('ec_product_convert_'. $type);
  }

  foreach (array('ec_product_cart_addition_by_link', 'ec_product_cart_on_teaser', 'ec_product_fresh_install') as $key) {
    variable_del($key);
  }

  if (node_get_types('product')) {
    drupal_set_message(t('The Product content type still exists. To remove this content type, Go to !url and delete it.', array('!url' => l(t('here', 'admin/content/node-type/product/delete')))));
  }
}

/**
 * Implementation of hook_schema().
 */
function ec_product_schema() {
  $schema = array();

  $schema['ec_product'] = array(
    'fields' => array(
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'disp-width' => '10',
      ),
      'vid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'disp-width' => '10',
      ),
      'ptype' => array(
        'type' => 'varchar',
        'length' => '75',
        'not null' => TRUE,
        'default' => '',
      ),
      'sku' => array(
        'type' => 'varchar',
        'length' => '75',
        'not null' => FALSE,
      ),
      'price' => array(
        'type' => 'numeric',
        'not null' => TRUE,
        'default' => 0,
        'precision' => '10',
        'scale' => '2',
      ),
    ),
    'unique keys' => array(
      'vid' => array('vid')),
    'indexes' => array(
      'ptype' => array('ptype')),
  );

  $schema['ec_product_ptypes'] = array(
    'fields' => array(
      'ptype' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
      ),
      'description' => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('ptype'),
  );

  $schema['ec_product_features'] = array(
    'fields' => array(
      'ptype' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
      ),
      'ftype' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
      ),
      'data' => array(
        'type' => 'text',
        'serialize' => TRUE,
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'disp-width' => '10',
       ),
      ),
    'primary key' => array('ptype', 'ftype'),
  );

  return $schema;
}

function ec_product_enable() {
  if (variable_get('ec_product_fresh_install', FALSE)) {
    $ptypes = array(
      array(
        'ptype'       => 'generic',
        'name'        => 'Generic Product',
        'description' => 'A non-shippable item has no inventory management. Examples are service based items.',
      ),
      array(
        'ptype'       => 'tangible',
        'name'        => 'Shippable Product',
        'description' => 'A shippable product is a tangible, physical item. Optional features includes inventory control and availability estimates.',
      ),
    );
    foreach ($ptypes as $ptype) {
      ec_product_ptypes_save((object)$ptype, FALSE);
    }

    $record = array('ptype' => 'tangible', 'ftype' => 'shippable');
    drupal_write_record('ec_product_features', $record);
    _ec_product_create_node_type();
    variable_del('ec_product_fresh_install');
  }
  $versions = drupal_get_installed_schema_version('product', FALSE, TRUE);
  if (isset($versions['product']) && $versions['product'] != SCHEMA_UNINSTALLED) {
    // Upgrade from e-Commerce 3 product module.
    drupal_set_installed_schema_version('ec_product', $versions['product']);
    drupal_set_installed_schema_version('product', SCHEMA_UNINSTALLED);
  }
}

function _ec_product_create_node_type() {
  $info = new StdClass;
  $info->type = 'product';
  $info->name = 'Product';
  $info->module = 'node';
  $info->has_title = 1;
  $info->title_label = 'Title';
  $info->has_body = 1;
  $info->body_label = 'Description';
  $info->description = 'A product is a good or service that you wish to sell on your site.';
  $info->help = '';
  $info->min_word_count = 0;
  $info->custom = 1;
  $info->modified = 1;
  $info->locked = 0;
  $info->orig_type = '';
  node_type_save($info);

  // Set product node type as "always" a product
  variable_set('ec_product_convert_product', 1);

  // Link all product types to product node type
  if ($ptypes = ec_product_get_types('names')) {
    variable_set('ec_product_ptypes_product', array_keys(ec_product_get_types('names')));
  }
}

/**
 * Check that the ec_anon updates have past 6400
 */
function ec_product_update_5400() {
  $ret = array();
  $version = drupal_get_installed_schema_version('ec_anon', TRUE);
  
  if ($version < 6400) {
    $ret['#abort'] = array('success' => FALSE, 'query' => t('Waiting until eC Anonymous has completed the initial upgrade'));
    return $ret;
  }
}

function ec_product_update_5401() {
  $schema = ec_product_schema();
  if (!db_table_exists('ec_product_ptypes')) {
    db_create_table($ret, 'ec_product_ptypes', $schema['ec_product_ptypes']);
  }

  $ptypes = array(
    array(
      'ptype'       => 'generic',
      'name'        => 'Generic Product',
      'description' => 'A non-shippable item has no inventory management. Examples are service based items.',
    ),
    array(
      'ptype'       => 'tangible',
      'name'        => 'Shippable Product',
      'description' => 'A shippable product is a tangible, physical item. Optional features includes inventory control and availability estimates.',
    ),
  );

  foreach ($ptypes as $ptype) {
    if (db_result(db_query_range("SELECT COUNT(*) FROM {ec_product_ptypes} WHERE ptype = '%s'", $ptype['ptype'], 0, 1))) {
      $ret[] = update_sql("UPDATE {ec_product_ptypes} SET name = '$ptype[name]', description = '$ptype[description]' WHERE ptype = '$ptype[ptype]'");
    }
    else {
      $ret[] = update_sql("INSERT INTO {ec_product_ptypes} (name, ptype, description) VALUES ('$ptype[name]', '$ptype[ptype]', '$ptype[description]')");
    }
  }
  return $ret;
}

function ec_product_update_5402() {
  $schema = ec_product_schema();
  if (!db_table_exists('ec_product_features')) {
    db_create_table($ret, 'ec_product_features', $schema['ec_product_features']);
  }

  $ptypes = array(
    array(
      'ptype'       => 'generic',
      'name'        => 'Generic Product',
      'description' => 'A non-shippable item has no inventory management. Examples are service based items.',
    ),
    array(
      'ptype' => 'tangible',
      'name' => 'Shippable Product',
      'description' => 'A shippable product is a tangible, physical item.',
    ),
  );

  drupal_load('module', 'ec_product');

  foreach ($ptypes as $ptype) {
    ec_product_ptypes_save((object)$ptype);
  }
  $record = array('ptype' => 'tangible', 'ftype' => 'shippable');
  drupal_write_record('ec_product_features', $record);
  return $ret;
}

/**
 * All custom nodes are never converted to products by default.
 * We should invert all optional to never and never to optional
 */
function ec_product_update_5403() {
  foreach (node_get_types('names') as $type => $name) {
    if (variable_get('ec_product_convert_'. $type, 1) == 0) {
      variable_set('ec_product_convert_'. $type, 2);
    }
    elseif (variable_get('ec_product_convert_'. $type, 1) == 2) {
      variable_set('ec_product_convert_'. $type, 0);
    }
  }
  return array();
}

/**
 * Remove 5404 as it is now being handled above
 */

/**
 * Create new node type product
 */
function ec_product_update_5405() {
  $ret = array();
  node_types_rebuild(); // Delete the node type product.

  _ec_product_create_node_type();

  return $ret;
}

/**
 * Add Data field to the ec_product_features tables
 */
function ec_product_update_6401() {
  $ret = array();
  if (!db_column_exists('ec_product_features', 'data')) {
    db_add_field($ret, 'ec_product_features', 'data', array('type' => 'text', 'serialize' => TRUE));
  }
  return $ret;
}

/**
 * Fix up the default value for the weight on the ec_product_feature to default to 0
 */
function ec_product_update_6402() {
  $ret = array();

  db_change_field($ret, 'ec_product_features', 'weight', 'weight', array(
    'type' => 'int',
    'not null' => TRUE,
    'default' => 0,
    'disp-width' => '10'
  ));

  return $ret;
}

/**
 * Add purchase products permission to anonymous an registered user roles.
 */
function ec_product_update_6403() {
  $ret = array();

  $result = db_query('SELECT * FROM {permission} WHERE rid in (1,2)');
  while ($perm = db_fetch_array($result)) {
    if (stripos(', '. $perm['perm'], ', purchase products') === FALSE) {
      $perm['perm'] .= ', purchase products';
      drupal_write_record('permission', $perm, 'pid');
    }
  }
  return array();
}

/**
 * Adds column to ec_product table to for anonymous policy
 */
function ec_product_update_6404() {
  $ret = array();
  if (db_table_exists('ec_product') && !db_column_exists('ec_product', 'anon_policy')) {
    db_add_field($ret, 'ec_product', 'anon_policy', array('type' => 'int', 'size' => 'tiny', 'not null' => TRUE, 'default' => 0));
  }
  return array();
}
