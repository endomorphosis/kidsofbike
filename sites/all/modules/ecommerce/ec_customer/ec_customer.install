<?php

/**
 * @file
 * Install and uninstall routines, incremental database updates and database
 * schema.
 */

/**
 * Implementation of hook_requirements().
 */
function ec_customer_requirements($phase) {
  $requirements = array();

  if ($phase == 'runtime') {
    $requirements['ec_customer_types'] = array(
      'title' => t('Customer types'),
      'description' => t('Customer types are ways of defining different customers such as anonymous, users, etc.'),
    );
    if ($ctypes = ec_customer_get_types('names')) {
      if (count($ctypes) == 1 && isset($ctypes['anonymous'])) {
        $requirements['ec_customer_types'] += array(
          'value' => t('Only anonymous customers can be sold to. You will need to enable another customer module to enable be able to sell to other customer types.'),
          'severity' => REQUIREMENT_WARNING,
        );
      }
      else {
        $requirements['ec_customer_types'] += array(
          'value' => t('Enabled customer types: @customer_type', array('@customer_type' => implode(', ', $ctypes))),
          'severity' => REQUIREMENT_OK,
        );
      }
    }
    else {
      $requirements['ec_customer_types'] += array(
        'value' => t('There are currently no customer types enables.'),
        'severity' => REQUIREMENT_ERROR,
      );
    }
  }

  return $requirements;
}

/**
 * Implementation of hook_install().
 */
function ec_customer_install() {
  drupal_install_schema('ec_customer');
}

/**
 * Implementation of hook_uninstall().
 */
function ec_customer_uninstall() {
  drupal_uninstall_schema('ec_customer');
}

/**
 * Implementation of hook_schema().
 */
function ec_customer_schema() {
  $schema = array();

  $schema['ec_customer'] = array(
    'fields' => array(
      'ecid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'disp-width' => '10'
      ),
      'type' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE
      ),
      'exid' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE
      ),
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'User ID.',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => ''
      ),
      'token' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => ''
      ),
    ),
    'primary key' => array('ecid'),
    'unique keys' => array(
       'type' => array('type', 'exid')
    ),
  );

  return $schema;
}

/**
 * Check that the ec_anon updates have past 6400
 */
function ec_customer_update_6400() {
  $ret = array();
  $version = drupal_get_installed_schema_version('ec_anon', TRUE);
  
  if ($version < 6400) {
    $ret['#abort'] = array('success' => FALSE, 'query' => t('Waiting until eC Anonymous has completed the initial upgrade'));
    return $ret;
  }
}

/**
 * Change ecid so that it is a auto incrementing field instead of just an
 * int.
 */
function ec_customer_update_6401() {
  $ret = array();

  db_change_field($ret, 'ec_customer', 'ecid', 'ecid', array('type' => 'serial', 'unsigned' => TRUE, 'not null' => TRUE, 'disp-width' => '10'));

  return $ret;
}

/**
 * Create new fields on the ec_customer table
 */
function ec_customer_update_6402() {
  $ret = array();

  db_add_field($ret, 'ec_customer', 'uid', array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'description' => 'user ID.'));
  db_add_field($ret, 'ec_customer', 'name', array('type' => 'varchar', 'length' => '255', 'not null' => TRUE, 'default' => ''));

  return $ret;
}

/**
 * Generation both uid and name fields for all current customers.
 */
function ec_customer_update_6403(&$sandbox) {
  $ret = array();
  if (empty($sandbox)) {
    $sandbox['max'] = db_result(db_query('SELECT MAX(ecid) FROM {ec_customer}'));
    $sandbox['last'] = 0;
  }

  $result = db_query('SELECT * FROM {ec_customer} WHERE ecid > %d ORDER BY ecid ASC', $sandbox['last']);

  while ($customer = db_fetch_object($result)) {
    $customer->uid = ec_customer_get_uid($customer);
    $customer->name = ec_customer_get_name($customer);
    drupal_write_record('ec_customer', $customer, 'ecid');

    $sandbox['last'] = $customer->ecid;

    if (timer_read('batch_processing') > 1000) {
      break;
    }
  }

  if ($sandbox['last'] < $sandbox['max']) {
    $ret['#finished'] = $sandbox['last']/$sandbox['max'];
  }
  else {
    $ret['#finished'] = 1;
  }

  return $ret;
}

